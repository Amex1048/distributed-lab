use std::io::Write;

use bevy::prelude::*;
use bevy::utils::Uuid;

use time::OffsetDateTime;

use crate::peer_to_peer::{Counter, Network, PeerDescription};
use crate::ui::{NewPeerEvent, SetActivePeerEvent};

pub struct ServerPlugin;
impl Plugin for ServerPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<SelfDescription>()
            .init_resource::<HttpClient>()
            .add_event::<SetupEvent>()
            .add_system(setup);
    }
}

#[derive(Debug, Clone, Resource)]
pub struct SelfDescription {
    pub address: String,
    pub id: Option<usize>,
}

impl Default for SelfDescription {
    fn default() -> Self {
        let response = reqwest::blocking::get("https://api.ipify.org").unwrap();
        let ip = response.text().unwrap();
        // let ip = serde_json::to_value(response.text().unwrap()).unwrap()["ip"].to_string();
        // let ip = "localhost";
        let port = std::env::var("PORT").unwrap_or("5555".to_owned());

        Self {
            address: format!("{ip}:{port}"),
            id: None,
        }
    }
}
#[derive(Debug, Clone, serde::Deserialize)]
struct ServerResponse {
    your_id: usize,
    peers: Vec<crate::peer_to_peer::PeerDescription>,
}

#[derive(Debug)]
pub struct SetupEvent {
    pub address: String,
}

#[derive(Debug, Clone, Resource)]
pub struct HttpClient(pub reqwest::blocking::Client, pub Option<String>);

impl Default for HttpClient {
    fn default() -> Self {
        HttpClient(reqwest::blocking::Client::new(), None)
    }
}

#[derive(Debug, Clone, serde::Serialize)]
pub struct LogMessage {
    pub id: Uuid,
    pub time_stamp: OffsetDateTime,
    pub clock: Counter,
    pub from: String,
    pub to: String,
}

fn setup(
    mut setup_events: EventReader<SetupEvent>,
    mut http_client: ResMut<HttpClient>,
    mut self_description: ResMut<SelfDescription>,
    mut network: ResMut<Network>,
    mut new_peer_event: EventWriter<NewPeerEvent>,
    mut active_peer_evw: EventWriter<SetActivePeerEvent>,
) {
    for ev in setup_events.iter() {
        println!("{}", ev.address);
        println!("{:?}", self_description);

        http_client.1 = Some(ev.address.clone());
        let response = http_client
            .0
            .post(&format!("http://{}/setup", ev.address))
            .json(&serde_json::json!({ "address": self_description.address }))
            .send()
            .unwrap();
        assert!(response.status().is_success());

        let text = response.text().unwrap();
        println!("{}", text);

        let response: ServerResponse = serde_json::from_str(&text).unwrap();

        self_description.id = Some(response.your_id);

        network.peers = response
            .peers
            .into_iter()
            .filter(|desc| desc.address != self_description.address)
            .enumerate()
            .map(|(i, desc)| {
                let mut socket = std::net::TcpStream::connect(&desc.address).unwrap();
                println!("Created socket");
                socket.set_nonblocking(true).unwrap();
                socket
                    .write_all(
                        serde_json::to_string(&PeerDescription {
                            address: self_description.address.clone(),
                            id: self_description.id.unwrap(),
                        })
                        .unwrap()
                        .as_ref(),
                    )
                    .unwrap();
                println!("Self description sent");

                // black magic
                new_peer_event.send(NewPeerEvent(i, format!("{}-{}", desc.id, desc.address)));

                (
                    desc.id,
                    crate::peer_to_peer::Peer {
                        socket,
                        description: desc,
                    },
                )
            })
            .collect();

        active_peer_evw.send(SetActivePeerEvent(
            *network.peers.keys().next().unwrap_or(&1),
        ));
        // network.active_to_send = *network.peers.keys().next().unwrap_or(&1);
    }
}
