use bevy::prelude::*;

// mod net;
mod peer_to_peer;
mod server;
mod ui;

fn main() {
    App::new()
        .add_plugin(server::ServerPlugin)
        .add_plugin(peer_to_peer::PeerToPeerPlugin)
        .add_plugin(ui::UiPlugin)
        .run()
}
