use crate::peer_to_peer::{Network, RecvEvent, SendEvent};
use crate::server::SetupEvent;
use bevy::prelude::*;

pub struct NewPeerEvent(pub usize, pub String);
pub struct ConnectionFailedEvent;

pub struct SetActivePeerEvent(pub usize);

struct InfoMessageEvent(String);

pub struct UiPlugin;

impl Plugin for UiPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<NewPeerEvent>()
            .add_event::<ConnectionFailedEvent>()
            .add_event::<InfoMessageEvent>()
            .add_event::<SetActivePeerEvent>()
            .add_plugins(DefaultPlugins.set(WindowPlugin {
                primary_window: Some(Window {
                    title: "Rust Bevy Sockets Chat".to_string(),
                    resize_constraints: WindowResizeConstraints {
                        min_width: 400.0,
                        min_height: 300.0,
                        max_width: 800.0,
                        max_height: 600.0,
                    },
                    ..default()
                }),
                ..default()
            }))
            .add_startup_system(spawn_ui)
            .add_system(type_and_submit_message)
            .add_system(log_new_peer)
            .add_system(set_active_peer)
            .add_system(add_new_peer_to_list.before(set_active_peer))
            // .add_system(toggle_peers_by_click)
            .add_system(log_incoming_message)
            .add_system(log_outgoing_message)
            .add_system(log_info_message);
    }
}

fn type_and_submit_message(
    mut char_evr: EventReader<ReceivedCharacter>,
    mut q: Query<&mut Text, With<MessageInput>>,
    mut submit_evw: EventWriter<SendEvent>,
    // mut connect_evw: EventWriter<ConnectEvent>,
    mut setup_evw: EventWriter<SetupEvent>,
    mut active_peer_evw: EventWriter<SetActivePeerEvent>,
) {
    const BACKSPACE: char = '\x08';
    const RETURN: char = '\x0D';
    let mut ui_text = q.single_mut();
    for char_ev in char_evr.iter() {
        match char_ev.char {
            BACKSPACE => _ = ui_text.sections[0].value.pop(),
            RETURN => {
                let text = ui_text.sections[0].value.clone();
                ui_text.sections[0].value = String::from("");
                // if text.starts_with("connect ") {
                //     connect_evw.send(ConnectEvent {
                //         address: text.strip_prefix("connect ").unwrap().to_string(),
                //     });
                // } else if text.starts_with("setup ") {
                if text.starts_with("/setup ") {
                    setup_evw.send(SetupEvent {
                        address: text.strip_prefix("/setup ").unwrap().to_string(),
                    });
                } else if text.starts_with("/set") {
                    active_peer_evw.send(SetActivePeerEvent(
                        text.strip_prefix("/set ").unwrap().parse().unwrap(),
                    ));
                } else {
                    submit_evw.send(SendEvent {
                        uuid: None,
                        to: None,
                        message: text,
                    });
                }
            }
            c => ui_text.sections[0].value.push(c),
        }
    }
}

fn log_new_peer(mut evr: EventReader<NewPeerEvent>, mut evw: EventWriter<InfoMessageEvent>) {
    for ev in evr.iter() {
        evw.send(InfoMessageEvent(format!("new peer connected: {}", ev.1)));
    }
}

#[derive(Component)]
struct ListOfPeersItem(usize);
fn add_new_peer_to_list(
    mut evr: EventReader<NewPeerEvent>,
    mut cmd: Commands,
    q: Query<Entity, With<ListOfPeers>>,
    font: Res<MyFont>,
) {
    let ui_parent = q.single();
    for ev in evr.iter() {
        let ui_item = cmd
            .spawn(NodeBundle {
                style: Style {
                    flex_grow: 0.0,
                    flex_shrink: 0.0,
                    ..default()
                },
                ..default()
            })
            .with_children(|parent| {
                parent.spawn((
                    ListOfPeersItem(ev.0),
                    Interaction::default(),
                    TextBundle::from_section(
                        ev.1.clone(),
                        TextStyle {
                            font: font.0.clone(),
                            font_size: 19.0,
                            color: Color::SILVER,
                        },
                    ),
                ));
            })
            .id();
        cmd.entity(ui_parent).push_children(&[ui_item]);
    }
}

fn set_active_peer(
    mut q: Query<(&mut Text, &Interaction, &ListOfPeersItem)>,
    mut active_peer_evw: EventReader<SetActivePeerEvent>,
) {
    for ev in active_peer_evw.iter() {
        println!("updating ui with {}", ev.0);
        for (mut ui_text, _, _) in q.iter_mut() {
            let id: usize = ui_text.sections[0]
                .value
                .split_once('-')
                .unwrap()
                .0
                .parse()
                .unwrap();
            println!("id: {}", id);
            if ev.0 == id {
                ui_text.sections[0].style.color = Color::BLACK;
            } else {
                ui_text.sections[0].style.color = Color::SILVER;
            }
            // if *interaction == Interaction::Clicked {
            //     if ui_text.sections[0].style.color == Color::BLACK {
            //         ui_text.sections[0].style.color = Color::SILVER;
            //         // peers.0[list_of_peers_index.0].0 = false;
            //     } else {
            //         ui_text.sections[0].style.color = Color::BLACK;
            //         // peers.0[list_of_peers_index.0].0 = true;
            //     }
            // }
        }
    }
}

// fn toggle_peers_by_click(
//     mut _q: Query<(&mut Text, &Interaction, &ListOfPeersItem), Changed<Interaction>>,
//     mut _peers: ResMut<Network>,
// ) {
// for (mut ui_text, interaction, list_of_peers_index) in &mut q {
//     if *interaction == Interaction::Clicked {
//         if ui_text.sections[0].style.color == Color::BLACK {
//             ui_text.sections[0].style.color = Color::SILVER;
//             peers.0[list_of_peers_index.0].0 = false;
//         } else {
//             ui_text.sections[0].style.color = Color::BLACK;
//             peers.0[list_of_peers_index.0].0 = true;
//         }
//     }
// }
// }

// fn log_connection_failed(
//     mut evr: EventReader<ConnectionFailedEvent>,
//     mut evw: EventWriter<InfoMessageEvent>,
// ) {
//     for _ in evr.iter() {
//         evw.send(InfoMessageEvent("connection failed".to_string()));
//     }
// }

fn log_incoming_message(
    mut evr: EventReader<RecvEvent>,
    mut q: Query<&mut Text, With<MessageLog>>,
    font: Res<MyFont>,
) {
    let mut ui_text = q.single_mut();
    for ev in evr.iter() {
        ui_text.sections.push(TextSection::new(
            format!("from {}: ", ev.address),
            TextStyle {
                font: font.0.clone(),
                font_size: 19.0,
                color: Color::GRAY,
            },
        ));
        ui_text.sections.push(TextSection::new(
            format!("{}\n", ev.message),
            TextStyle {
                font: font.0.clone(),
                font_size: 19.0,
                color: Color::BLACK,
            },
        ));
    }
}

fn log_outgoing_message(
    mut evr: EventReader<SendEvent>,
    mut q: Query<&mut Text, With<MessageLog>>,
    font: Res<MyFont>,
) {
    let mut ui_text = q.single_mut();
    for ev in evr.iter() {
        ui_text.sections.push(TextSection::new(
            "you: ".to_string(),
            TextStyle {
                font: font.0.clone(),
                font_size: 19.0,
                color: Color::GRAY,
            },
        ));
        ui_text.sections.push(TextSection::new(
            format!("{}\n", ev.message),
            TextStyle {
                font: font.0.clone(),
                font_size: 19.0,
                color: Color::BLACK,
            },
        ));
    }
}

fn log_info_message(
    mut evr: EventReader<InfoMessageEvent>,
    mut q: Query<&mut Text, With<MessageLog>>,
    font: Res<MyFont>,
) {
    let mut ui_text = q.single_mut();
    for ev in evr.iter() {
        ui_text.sections.push(TextSection::new(
            format!("{}\n", ev.0),
            TextStyle {
                font: font.0.clone(),
                font_size: 19.0,
                color: Color::GRAY,
            },
        ));
    }
}

#[derive(Component)]
struct MessageLog;
#[derive(Component)]
struct MessageInput;
#[derive(Component)]
struct ListOfPeers;
#[derive(Resource)]
struct MyFont(Handle<Font>);
fn spawn_ui(mut cmd: Commands, asset_server: Res<AssetServer>) {
    #[cfg(target_os = "linux")]
    const FONT_PATH: &str = "/usr/share/fonts/noto/NotoSans-Regular.ttf";
    #[cfg(target_os = "windows")]
    const FONT_PATH: &str = "C:/Windows/Fonts/arial.ttf";

    let font_handle = asset_server.load(FONT_PATH);
    cmd.insert_resource(MyFont(font_handle.clone()));
    cmd.spawn(Camera2dBundle::default());
    cmd.spawn(NodeBundle {
        background_color: BackgroundColor(Color::SILVER),
        style: Style {
            size: Size::all(Val::Percent(100.0)),
            padding: UiRect::all(Val::Px(20.0)),
            flex_direction: FlexDirection::Column,
            gap: Size::all(Val::Px(20.0)),
            ..default()
        },
        ..default()
    })
    .with_children(|parent| {
        parent
            .spawn(NodeBundle {
                style: Style {
                    flex_shrink: 1.0,
                    flex_grow: 1.0,
                    gap: Size::all(Val::Px(20.0)),
                    ..default()
                },
                ..default()
            })
            .with_children(|parent| {
                parent
                    .spawn(NodeBundle {
                        background_color: BackgroundColor(Color::WHITE),
                        style: Style {
                            padding: UiRect::all(Val::Px(10.0)),
                            flex_shrink: 1.0,
                            flex_grow: 1.0,
                            ..default()
                        },
                        ..default()
                    })
                    .with_children(|parent| {
                        parent.spawn((MessageLog, TextBundle::from_sections([])));
                    });
                parent.spawn((
                    ListOfPeers,
                    NodeBundle {
                        background_color: BackgroundColor(Color::WHITE),
                        style: Style {
                            flex_shrink: 0.0,
                            flex_grow: 0.0,
                            padding: UiRect::all(Val::Px(10.0)),
                            size: Size::width(Val::Px(250.0)),
                            flex_direction: FlexDirection::Column,
                            gap: Size::all(Val::Px(3.0)),
                            ..default()
                        },
                        ..default()
                    },
                ));
            });
        parent
            .spawn(NodeBundle {
                background_color: BackgroundColor(Color::WHITE),
                style: Style {
                    flex_shrink: 0.0,
                    flex_grow: 0.0,
                    padding: UiRect::all(Val::Px(10.0)),
                    size: Size::height(Val::Px(50.0)),
                    justify_content: JustifyContent::FlexStart,
                    align_items: AlignItems::Center,
                    ..default()
                },
                ..default()
            })
            .with_children(|parent| {
                parent.spawn((
                    MessageInput,
                    TextBundle::from_section(
                        "",
                        TextStyle {
                            font: font_handle.clone(),
                            font_size: 19.0,
                            color: Color::BLACK,
                        },
                    ),
                ));
            });
    });
}
