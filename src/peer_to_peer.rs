use crate::server::{HttpClient, LogMessage, SelfDescription};
use crate::ui::{NewPeerEvent, SetActivePeerEvent};

use std::io::{Read, Write};
use std::net::TcpStream;

use bevy::prelude::*;
use bevy::utils::Uuid;

pub struct PeerToPeerPlugin;
impl Plugin for PeerToPeerPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<Network>()
            .init_resource::<ConnectionsListener>()
            .init_resource::<Counter>()
            .init_resource::<InstanceMessages>()
            .add_event::<RecvEvent>()
            .add_event::<SendEvent>()
            .add_system(accept)
            .add_system(send)
            .add_system(recv)
            .add_system(update_active_peer);
    }
}

#[derive(Debug, Resource, Default)]
pub struct Network {
    pub peers: std::collections::HashMap<usize, Peer>,
    pub active_to_send: usize,
}

#[derive(Debug, Clone, serde::Deserialize, serde::Serialize)]
pub struct PeerDescription {
    pub address: String,
    pub id: usize,
}

#[derive(Debug)]
pub struct Peer {
    pub description: PeerDescription,
    pub socket: TcpStream,
}

#[derive(Debug, Resource)]
struct ConnectionsListener(std::net::TcpListener);

impl Default for ConnectionsListener {
    fn default() -> Self {
        let port = std::env::var("PORT").unwrap_or("5555".to_owned());
        let listener = std::net::TcpListener::bind(format!("0.0.0.0:{port}")).unwrap();
        listener.set_nonblocking(true).unwrap();
        Self(listener)
    }
}

#[derive(Debug, Clone, Resource, Default, serde::Deserialize, serde::Serialize)]
#[serde(transparent)]
pub struct Counter(usize);

#[derive(Debug, Clone, Resource, Default)]
struct InstanceMessages(Vec<Uuid>);

fn accept(
    listener: Res<ConnectionsListener>,
    mut network: ResMut<Network>,
    mut new_peer_event: EventWriter<NewPeerEvent>,
    mut active_peer_evw: EventWriter<SetActivePeerEvent>,
) {
    // println!("accepting...");
    while let Ok((mut sock, _)) = listener.0.accept() {
        println!("accepted...");
        let peer_desc: PeerDescription = {
            let mut buf = [0; 512];
            let size = sock.read(&mut buf).unwrap();
            serde_json::from_slice(&buf[..size]).unwrap()
            // sock.read_to_string(&mut buf).unwrap();
            // serde_json::from_slice(&buf.split(|&x| x == b'\n').next().unwrap()).unwrap()
        };
        println!("accepted info...");

        sock.set_nonblocking(true).unwrap();
        // network.peers.push(Peer {
        //     description: peer_desc.clone(),
        //     socket: sock,
        // });
        network.peers.insert(
            peer_desc.id,
            Peer {
                description: peer_desc.clone(),
                socket: sock,
            },
        );

        // black magic
        new_peer_event.send(NewPeerEvent(
            network.peers.len() - 1,
            format!("{}-{}", peer_desc.id, peer_desc.address),
        ));

        if network.peers.len() == 1 {
            active_peer_evw.send(SetActivePeerEvent(peer_desc.id));
        }
    }
}

pub struct SendEvent {
    pub uuid: Option<Uuid>,
    pub to: Option<usize>,
    pub message: String,
}

pub struct RecvEvent {
    pub address: String,
    pub message: String,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
struct Message {
    uuid: Uuid,
    counter: Counter,
    message: String,
}

fn send(
    mut evr: EventReader<SendEvent>,
    http_client: Res<HttpClient>,
    mut network: ResMut<Network>,
    mut counter: ResMut<Counter>,
    mut instance_messages: ResMut<InstanceMessages>,
    self_description: Res<SelfDescription>,
) {
    for ev in evr.iter() {
        counter.0 += 1;

        let message = Message {
            uuid: ev.uuid.unwrap_or(Uuid::new_v4()),
            counter: counter.clone(),
            message: ev.message.clone(),
        };

        let send_to = ev.to.unwrap_or(network.active_to_send);

        println!("sending message");

        let send = network.peers.get_mut(&send_to).unwrap();
        send.socket
            .write_all(serde_json::to_string(&message).unwrap().as_ref())
            .unwrap();

        println!("message sent");

        instance_messages.0.push(message.uuid);

        let log = serde_json::json!(
        {
            "id": message.uuid,
            "time_stamp": time::OffsetDateTime::now_utc(),
            "clock": counter.clone(),
            "from": format!(
                "{}-{}",
                self_description.id.unwrap(),
                self_description.address
            ),
            "to": format!("{}-{}", send.description.id, send.description.address),
        });

        // print!("send log: {log}");

        let response = http_client
            .0
            .post(&format!("http://{}/log", http_client.1.as_ref().unwrap()))
            .json(&log)
            .send()
            .unwrap();
        assert!(response.status().is_success());
    }
}

fn recv(
    mut network: ResMut<Network>,
    mut recv_event: EventWriter<RecvEvent>,
    mut counter: ResMut<Counter>,
    mut instance_messages: ResMut<InstanceMessages>,
    mut proxy_send: EventWriter<SendEvent>,
    self_description: Res<SelfDescription>,
    http_client: Res<HttpClient>,
) {
    let peers = network.peers.len() + 1;
    for peer in network.peers.values_mut() {
        // let mut message = String::new();
        let mut message = [0; 512];
        if let Ok(x) = peer.socket.read(&mut message) {
            if x > 0 {
                let message: Message = serde_json::from_slice(&message[..x]).unwrap();
                *counter = Counter(message.counter.0.max(counter.0) + 1);

                recv_event.send(RecvEvent {
                    address: peer.description.address.clone(),
                    message: message.message.clone(),
                });

                if instance_messages.0.contains(&message.uuid) {
                    instance_messages.0.retain(|&x| x != message.uuid);
                } else if (peer.description.id + 1) % peers
                    == self_description.id.unwrap_or_default()
                {
                    proxy_send.send(SendEvent {
                        uuid: Some(message.uuid),
                        to: self_description.id.map(|id| (id + 1) % peers),
                        message: message.message,
                    })
                }

                let response = http_client
                    .0
                    .post(&format!("http://{}/log", http_client.1.as_ref().unwrap()))
                    .json(&serde_json::json!(
                    {
                        "id": message.uuid,
                        "time_stamp": time::OffsetDateTime::now_utc(),
                        "clock": counter.clone(),
                        "from": format!(
                            "{}-{}",
                            peer.description.id,
                            peer.description.address
                        ),
                        "to": format!("{}-{}", self_description.id.unwrap(), self_description.address),
                    }))
                    .send()
                    .unwrap();
                assert!(response.status().is_success());
            }
        }
    }
}

fn update_active_peer(mut network: ResMut<Network>, mut event: EventReader<SetActivePeerEvent>) {
    for ev in event.iter() {
        if network.peers.contains_key(&ev.0) {
            println!("updating network with {}", ev.0);
            network.active_to_send = ev.0;
        }
    }
}
