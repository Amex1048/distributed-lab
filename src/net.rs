use crate::ui::{ConnectionFailedEvent, NewPeerEvent};
use bevy::prelude::*;
use std::io::prelude::*;
use std::net::{TcpListener, TcpStream};

pub struct SendEvent {
    pub message: String,
}

pub struct RecvEvent {
    pub address: String,
    pub message: String,
}

pub struct ConnectEvent {
    pub address: String,
}

pub struct SetupEvent {
    pub address: String,
}

pub struct NetPlugin;
impl Plugin for NetPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<Listener>()
            .init_resource::<Peers>()
            .add_event::<SetupEvent>()
            .add_event::<ConnectEvent>()
            .add_event::<SendEvent>()
            .add_event::<RecvEvent>()
            .add_system(accept)
            .add_system(connect)
            .add_system(send)
            .add_system(recv);
    }
}

#[derive(Resource)]
struct Listener(TcpListener);
impl Default for Listener {
    fn default() -> Self {
        let port = std::env::var("PORT").unwrap_or("5555".to_string());
        let listener = TcpListener::bind(format!("127.0.0.1:{port}")).unwrap();
        listener.set_nonblocking(true).unwrap();
        Self(listener)
    }
}

#[derive(Default, Resource)]
pub struct Peers(pub Vec<(bool, TcpStream, String)>);

fn accept(
    listener: Res<Listener>,
    mut peers: ResMut<Peers>,
    mut new_peer_evw: EventWriter<NewPeerEvent>,
) {
    while let Ok((sock, _)) = listener.0.accept() {
        let addr = sock.peer_addr().unwrap();
        new_peer_evw.send(NewPeerEvent(peers.0.len(), addr.to_string()));
        sock.set_nonblocking(true).unwrap();
        peers.0.push((true, sock, addr.to_string()));
    }
}

fn connect(
    mut evr: EventReader<ConnectEvent>,
    mut peers: ResMut<Peers>,
    mut new_peer_evw: EventWriter<NewPeerEvent>,
    mut conn_fail_evw: EventWriter<ConnectionFailedEvent>,
) {
    for ev in evr.iter() {
        match TcpStream::connect(&ev.address) {
            Ok(sock) => {
                new_peer_evw.send(NewPeerEvent(
                    peers.0.len(),
                    sock.peer_addr().unwrap().to_string(),
                ));
                sock.set_nonblocking(true).unwrap();
                peers.0.push((true, sock, ev.address.clone()));
            }
            _ => {
                conn_fail_evw.send(ConnectionFailedEvent);
            }
        }
    }
}

fn send(mut evr: EventReader<SendEvent>, mut peers: ResMut<Peers>) {
    for ev in evr.iter() {
        let msg_buf = ev.message.as_bytes();
        for peer in &mut peers.0 {
            if peer.0 {
                peer.1.write_all(&msg_buf).unwrap();
            }
        }
    }
}

fn recv(mut peers: ResMut<Peers>, mut evw: EventWriter<RecvEvent>) {
    for peer in &mut peers.0 {
        let mut msg = [0; 64];
        if let Ok(x) = peer.1.read(&mut msg) {
            if x > 0 {
                evw.send(RecvEvent {
                    address: peer.2.clone(),
                    message: String::from_utf8_lossy(&msg).into_owned(),
                });
            }
        }
    }
}
